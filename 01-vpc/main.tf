terraform {
  backend "s3" {
    bucket = "bucket-stack-eks"
    key    = "key-stack-eks/vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  name                 = "vpc-stack-eks"
  region               = "us-east-1"
  cidr                 = "10.13.0.0/16"
  azs                  = ["${local.region}a", "${local.region}b"]
  private_subnets_cidr = ["10.13.10.0/24", "10.13.11.0/24"]
  public_subnets_cidr  = ["10.13.210.0/24", "10.13.211.0/24"]
  tags = {
    Owner       = "stack-eks"
    Environment = "dev"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/k8s-stack-eks" = "shared"
    "kubernetes.io/role/internal-elb"                      = "1"
    "tier"                                                 = "private"
  }
  public_subnet_tags = {
    "kubernetes.io/cluster/k8s-stack-eks" = "shared"
    "kubernetes.io/role/elb"                               = "1"
    "tier"                                                 = "public"
  }
}
