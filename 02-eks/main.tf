terraform {
  backend "s3" {
    bucket = "bucket-stack-eks"
    key    = "key-stack-eks/eks/terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  vpc_name = "vpc-stack-eks"

  cluster_name    = "k8s-stack-eks"
  cluster_version = "1.21"

  node_groups_key_name          = "k8s-key"
  node_groups_enable_monitoring = false
  node_groups_disk_size         = 30
  node_groups_force_update      = false
  node_groups_config = {
    # "system" = {
    #   name                   = "nodes-system-${random_string.suffix.result}"
    #   desired_capacity       = 4
    #   max_capacity           = 10
    #   min_capacity           = 4
    #   instance_types         = ["t3.large"]
    #   k8s_labels = {
    #     "node_name"         = "nodes-system"
    #     "node/reserved-for" = "system"
    #     "node_groups_force_update" = random_string.suffix.keepers.force_update
    #   }
    #   taints = [
    #     {
    #       key    = "node/reserved-for"
    #       value  = "system"
    #       effect = "NO_SCHEDULE"
    #     }
    #   ]
    #   additional_tags = {
    #     "Name"                                            = "${local.cluster_name}-nodes-system"
    #     "k8s.io/cluster-autoscaler/${local.cluster_name}" = "owned"
    #     "k8s.io/cluster-autoscaler/enabled"               = "TRUE"
    #     "Environment"                                     = "PROD"
    #   }
    #   update_config = {
    #     max_unavailable_percentage = 50
    #   }
    # },

    "default" = {
      name             = "nodes-default-${random_string.suffix.result}"
      desired_capacity = 2
      max_capacity     = 10
      min_capacity     = 2
      instance_types   = ["t3.large"]
      # instance_types         = ["t3.large","t3a.large","m5a.large","m5ad.large","m5dn.large","m5n.large","m4.large","m5.large","m5d.large","t2.large"]
      # capacity_type  = "SPOT"
      k8s_labels = {
        "node_name"                = "nodes-default"
        "node_groups_force_update" = random_string.suffix.keepers.force_update
      }
      additional_tags = {
        "Name"                                            = "${local.cluster_name}-nodes-default"
        "k8s.io/cluster-autoscaler/${local.cluster_name}" = "owned"
        "k8s.io/cluster-autoscaler/enabled"               = "TRUE"
        "Environment"                                     = "PROD"
      }
      update_config = {
        max_unavailable_percentage = 50 # or set `max_unavailable`
      }
    }
  }

  roles_aws_auth = [
    {
      rolearn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/manager-${local.cluster_name}_instance_role"
      username = "manager-k8s"
      groups   = ["system:masters"]
    },
    {
      rolearn  = "arn:aws:iam::920166752983:role/manager-k8s"
      username = "adminspoc"
      groups   = ["system:masters"]
    }
  ]

  users_aws_auth = [
    {
      userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/Bruno"
      username = "Bruno"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/Bruno2"
      username = "Bruno2"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/Bruno3"
      username = "Bruno3"
      groups   = ["system:masters"]
    }
  ]
}

data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = [local.vpc_name]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.vpc.id

  filter {
    name   = "tag:tier"
    values = ["private"]
  }
}

data "aws_caller_identity" "current" {}

resource "random_string" "suffix" {
  length  = 8
  special = false
  keepers = {
    force_update = local.node_groups_force_update
  }
}
