data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.18.0"

  cluster_name      = local.cluster_name
  cluster_version   = local.cluster_version
  vpc_id            = data.aws_vpc.vpc.id
  subnets           = data.aws_subnet_ids.private.ids
  enable_irsa       = true
  workers_role_name = "eks_cluster_workers_role_${local.cluster_name}"
  workers_group_defaults = {
    key_name          = local.node_groups_key_name
    enable_monitoring = local.node_groups_enable_monitoring
  }
  node_groups_defaults = {
    create_launch_template = true
    ami_type               = "AL2_x86_64"
    disk_size              = local.node_groups_disk_size
  }
  node_groups = local.node_groups_config
  map_roles   = local.roles_aws_auth
  map_users   = local.users_aws_auth
}

resource "aws_security_group_rule" "ssh_access" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [data.aws_vpc.vpc.cidr_block]
  security_group_id = module.eks.worker_security_group_id
}

data "aws_iam_policy_document" "worker_policy" {
  statement {
    actions   = ["sts:AssumeRole"]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "worker_policy" {
  name   = join("-", ["worker_policy", local.cluster_name])
  path   = "/"
  policy = data.aws_iam_policy_document.worker_policy.json
}
resource "aws_iam_role_policy_attachment" "worker_policy" {
  role       = module.eks.worker_iam_role_name
  policy_arn = aws_iam_policy.worker_policy.arn
}
