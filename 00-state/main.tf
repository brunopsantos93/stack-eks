provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket        = "bucket-stack-eks"
  acl           = "private"
}
